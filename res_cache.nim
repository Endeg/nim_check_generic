import tables

proc createValue[K, V](key: K): V = discard
proc releaseValue[K, V](value: V) = discard

proc get*[K, V](self: var Table[K, V], key: K): V =
  if self.hasKey(key):
    result = self[key]
  else:
    let value = createValue[K, V](key)
    self[key] = value
    result = value

proc release*[K, V](self: var Table[K, V]) =
  for key, value in self.pairs():
    echo("Unloading resource '" & $key & "'")
    releaseValue[K, V](value)
    self.del(key)
