import res_cache, texture, tables, hashes, math

type
  ImageKey* = tuple[textureName: string, num: int]

  Image* = object
    name: string
    tex: Texture
    num: int
    id: int

var
  images*: Table[ImageKey, Image]

proc createValue[ImageKey, Image](key: ImageKey): Image =
  let t = textures.get(key.textureName)
  result = Image(name: key.textureName, tex: t, num: key.num, id: random())
proc releaseValue[ImageKey, Image](value: Image) = echo("Releasing image: " & $value)

proc `$`(self: Image): string = "Image(" & $self.name & ", " & $self.tex & ", " & $self.num & ", " & $self.id & ")"

proc hash*(x: ImageKey): THash =
  result = x.textureName.hash !& x.num.hash
  result = !$result
