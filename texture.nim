import res_cache, tables, math

type
  Texture* = object
    name: string
    id: int

var
  textures*: Table[string, Texture]

proc createValue[string, Texture](key: string): Texture = Texture(name: key, id: random())
proc releaseValue[string, Texture](value: Texture) = echo("Releasing texture: " & $ value)

proc `$`*(self: Texture): string = "Texture(" & $self.name & ", " & $self.id & ")"
