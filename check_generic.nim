import texture, image, res_cache

when isMainModule:
  let
    tex = textures.get("test1")
    img = images.get(("test1", 10))

  echo("img: " & $img)
  echo("tex: " & $tex)

  images.release()
  textures.release()
